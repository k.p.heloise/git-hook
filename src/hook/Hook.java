package hook;
import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.Files;

public class Hook {
    public static void main(String[] args) throws IOException {
        String hookName = args[0];
        String gitCommand = args[1];
        String gitArguments = args[2];

        System.out.println("Hook name: " + hookName);
        System.out.println("Git command: " + gitCommand);
        System.out.println("Git arguments: " + gitArguments);

        if(gitCommand.equals("commit")) {
            String[] gitArgumentList = gitArguments.split(" ");
            String commitMessage = gitArgumentList[gitArgumentList.length-1];
            if(commitMessage.contains("bonjour monsieur")) {
                System.out.println("votre commit est accepté");
                System.exit(0);
            }
            else {
                System.out.println("apprend la politesse.");
                System.exit(1);
            }
        }
    }
}

